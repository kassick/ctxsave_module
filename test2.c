/****************************************************************************
 * File: "/home/kassick/Sources/so2t2/test1.c"
 * Created: "Sat Jun 11 19:25:03 2016"
 * Updated: "2016-06-11 21:06:02 kassick"
 * $Id$
 * Copyright (C) 2016, Rodrigo Kassick
 ****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>

#include "savectx.h"



struct _my_ctx {
    int i,j;
} my_ctx;


void handler(int sig) {
    save_context("com.test2", &my_ctx, sizeof(my_ctx) );
    printf("Saved\n");
    exit(0);
}

int main(int argc, char *argv[])
{
    signal(SIGUSR1, handler);
    if (argc > 1) {
        restore_context("com.test2", &my_ctx, sizeof(my_ctx));
        printf("Dados restaurados\n");
    } else {
        my_ctx.i = 0;
        my_ctx.j = 0;
    }

    for (; my_ctx.i < 100; my_ctx.i++) {
        for(; my_ctx.j < 10; my_ctx.j++){
            printf("%d %d\n", my_ctx.i, my_ctx.j);
        }
        sleep(1);
        my_ctx.j = 0;
    }
    return 0;
}
