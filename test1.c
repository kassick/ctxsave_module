/****************************************************************************
 * File: "/home/kassick/Sources/so2t2/test1.c"
 * Created: "Sat Jun 11 19:25:03 2016"
 * Updated: "2016-06-16 10:50:52 kassick"
 * $Id$
 * Copyright (C) 2016, Rodrigo Kassick
 ****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "savectx.h"
int main(int argc, char *argv[])
{
    char dados[256];
    bzero(dados, 256);

    if (argc > 1) {
        int ret = restore_context("com.abacate", dados, 256);
        if (ret < 0)
            printf("Sem dados a restaurar: %d\n", ret);
        else
            printf("Dados restaurados: %s\n", dados);

    } else {
        snprintf(dados, 256, "%s", "oiq eu sou um abacate");
        save_context("com.abacate", dados, 256);
    }
    return 0;
}
