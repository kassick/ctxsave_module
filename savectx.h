/****************************************************************************
 * File: "/home/kassick/Sources/so2t2/savectx.h"
 * Created: "Sat Jun 11 20:51:13 2016"
 * Updated: "2016-06-16 10:52:07 kassick"
 * $Id$
 * Copyright (C) 2016, Rodrigo Kassick
 ****************************************************************************/


#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

/*-----------------------------------------------------------------------------
 * Function: save_context
 * Description: Salva o contexto da aplicação no disposivo * /dev/saveme.
 *              Assume-se que não pode ocorrer erro
 *
 *---------------------------------------------------------------------------*/
static void save_context(const char *name, const void * data, size_t len) {
    size_t namelen;
    int i;
    FILE* savedev = fopen("/dev/saveme", "w");
    if (!savedev) {
        fprintf(stderr, "Could not open saveme device: %s", strerror(errno));
        return;
    }

    printf("Saving %lu\n", len);
    namelen = strlen(name);

    size_t buflen = 2*sizeof(size_t) + len + namelen;
    char * buf = malloc(buflen);
    char * cursor = buf;
    memcpy(cursor, &namelen, sizeof(size_t));
    cursor += sizeof(size_t);
    memcpy(cursor, &len, sizeof(size_t));
    cursor += sizeof(size_t);
    memcpy(cursor, name, namelen);
    cursor += namelen;
    memcpy(cursor, data, len);

    fwrite(buf, buflen, sizeof(char), savedev); // ignore the error

    free(buf);

    fclose(savedev);
}

/*-----------------------------------------------------------------------------
 * Function: restore_context
 * Description: Restaura um contexto com o nome dado.
 *
 *              Se ocorrer um erro ou se forem lidos menos bytes do que
 *              especificado em len, o valor do retorno será um número
 *              negativo. Caso contrário, 0 será retornado
 *---------------------------------------------------------------------------*/
static int restore_context(const char * name, void * data, ssize_t len) {
    FILE* restoredev = fopen("/dev/restoreme", "r+");
    if (!restoredev) {
        fprintf(stderr, "Could not open restoreme device: %s\n", strerror(errno));
        return -1;
    }

    // escreve COM o terminador
    fwrite(name, strlen(name) + 1, sizeof(char), restoredev);
    while (len > 0) {
        size_t r = fread(data, sizeof(char), len, restoredev);
        if (r <= 0) {
            fprintf(stderr, "Erro: %s", strerror(errno));
            break;
        }
        len -= r;
        data += r;
    }

    fclose(restoredev);

    return -1 * len;
}
