/****************************************************************************
 * File: "/home/kassick/Dropbox/Teaching/2016-1/INF0024 -- Sistemas Operacionais II -- PNA/Trabalho 2 -- Implementação Referência/ctxsave.c"
 * Created: "Sat Jun 11 11:40:12 2016"
 * Updated: "2016-06-15 22:19:13 kassick"
 * $Id$
 * Copyright (C) 2016, Rodrigo Kassick
 ****************************************************************************/

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/uaccess.h>
#include <linux/cdev.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/types.h>
#include <linux/mutex.h>
#include <linux/kfifo.h>
#include <linux/list.h>
#include <linux/string.h>

/* Based on parrot device */

#define DEVICE_NAME "ctxsave"
#define DEVICE_NAME_SAVEME "saveme"
#define DEVICE_NAME_RESTOREME "restoreme"
#define CLASS_NAME "ctxsave"

#define AUTHOR "rrr kkk"
#define DESCRIPTION "A context-saving device"
#define VERSION "0.1"

#define dbg(format, arg...) do { if (debug) pr_info(CLASS_NAME ": %s: " format, __FUNCTION__, ## arg); } while (0)
#define err(format, arg...) pr_err(CLASS_NAME ": " format, ## arg)
#define info(format, arg...) pr_info(CLASS_NAME ": " format, ## arg)
#define warn(format, arg...) pr_warn(CLASS_NAME ": " format, ## arg)


/* Module information */
MODULE_AUTHOR(AUTHOR);
MODULE_DESCRIPTION(DESCRIPTION);
MODULE_VERSION(VERSION);
MODULE_LICENSE("GPL");

/* Device variables */
static struct class* ctxsave_class = NULL;
static struct device* saveme_device = NULL;
static struct device* restoreme_device = NULL;

static int ctxsave_major;

static LIST_HEAD(saved_ctx_list);

static DEFINE_MUTEX(restoreme_device_mutex);

/* Module parameters that can be provided on insmod */
static bool debug = false; /* print extra debug info */
module_param(debug, bool, S_IRUGO | S_IWUSR);
MODULE_PARM_DESC(debug, "enable debug info (default: false)");

/* Um contexto salvo */
struct saved_ctx {
    struct list_head to_list;
    struct kfifo fifo;
    int len;
    char * id;
};

static
void saved_ctx_init(struct saved_ctx * ctx) {
    INIT_LIST_HEAD(&ctx->to_list);
    ctx->len = 0;
}

static
int saved_ctx_reserve(struct saved_ctx * ctx, size_t bytes) {
    if (kfifo_alloc(&ctx->fifo, bytes, GFP_KERNEL) != 0)
        return -1;
    ctx->len = bytes;
    return 0;
}

static
void saved_ctx_release(struct saved_ctx * ctx) {
    if (ctx->len > 0) {
        kfifo_free(&ctx->fifo);
        ctx->len = 0;
    }
}

static
struct saved_ctx * find_ctx(const char * id) {
    struct saved_ctx * ret = NULL;
    struct list_head * cur;
    list_for_each(cur, &saved_ctx_list) {
        struct saved_ctx * cur_ctx;
        cur_ctx = list_entry(cur, struct saved_ctx, to_list);
        if (!strcmp(id, cur_ctx->id)) {
            ret = cur_ctx;
            break;
        }
    }
    return ret;
}

static
void add_saved_context(struct saved_ctx * ctx) {
    list_add_tail(&ctx->to_list, &saved_ctx_list);
}

// Operações do dispositivo restoreme

static struct saved_ctx * cur_ctx_restore;
static int restoreme_device_open(struct inode * inode, struct file * filp) {
    dbg("");

    /* We must read the id and then write back the bytes */
    if ( ((filp->f_flags & O_ACCMODE) == O_RDONLY) ||
         ((filp->f_flags & O_ACCMODE) == O_WRONLY)) {
        warn("Wrong access to device restoreme\n");
        return -EACCES;
    }

    /* Ensure that only one process has access to our device at any one time
    * For more info on concurrent accesses, see http://lwn.net/images/pdf/LDD3/ch05.pdf */
    return mutex_lock_interruptible(&restoreme_device_mutex);
}

static int restoreme_device_close(struct inode* inode, struct file* filp)
{
    dbg("");
    if (cur_ctx_restore) {
        saved_ctx_release(cur_ctx_restore);
        kfree(cur_ctx_restore->id);
        kfree(cur_ctx_restore);
    }

    cur_ctx_restore = NULL;
    mutex_unlock(&restoreme_device_mutex);
    return 0;
}

static ssize_t restoreme_device_write(struct file* filp, const char __user *buffer, size_t length, loff_t* offset) {
    char * id = kmalloc(length, GFP_KERNEL);
    if (strncpy_from_user(id, buffer, length) < 0) {
        err("Error copying from user");
        kfree(id);
        return -EINVAL;
    }
    dbg("Looking for %s\n", id);

    cur_ctx_restore = find_ctx(id);
    if (cur_ctx_restore) {
        dbg("Found!");
        list_del(&cur_ctx_restore->to_list);
    } else {
        dbg("Not found\n");
    }

    kfree(id);
    return length;
}

static ssize_t restoreme_device_read(struct file* filp, char __user *buffer, size_t length, loff_t *offset) {
    int retval;
    size_t len;
    unsigned int copied;

    if (!cur_ctx_restore) {
        return -EINVAL;
    }

    len = (length < cur_ctx_restore->len? length : cur_ctx_restore->len);
    dbg("Sending %d bytes to user\n", (int)len);

    retval = kfifo_to_user(&cur_ctx_restore->fifo, buffer, len, &copied);

    cur_ctx_restore->len -= copied;

    dbg("Returning %d", (int)copied);

    return retval ? retval : copied;
}



/* saveme device functions */

static int saveme_device_open(struct inode * inode, struct file * filp) {
    dbg("");


    /* We must read the id and then write back the bytes */
    if ( ((filp->f_flags & O_ACCMODE) == O_RDONLY)
         || ((filp->f_flags & O_ACCMODE) == O_RDWR) ) {
        warn("Wrong access to device saveme\n");
        return -EACCES;
    }

    /* Ensure that only one process has access to our device at any one time
    * For more info on concurrent accesses, see http://lwn.net/images/pdf/LDD3/ch05.pdf */
    return mutex_lock_interruptible(&restoreme_device_mutex);
}

static int saveme_device_close(struct inode* inode, struct file* filp)
{
    dbg("");
    cur_ctx_restore = NULL;
    mutex_unlock(&restoreme_device_mutex);
    return 0;
}

static ssize_t saveme_device_write(struct file* filp, const char __user *buffer, size_t length, loff_t* offset) {
    int retval;
    size_t namelen, buflen;
    char * id; //
    int i;
    unsigned int copied;
    struct saved_ctx *cur_ctx;

    dbg("got %lu bytez\n", length);

    // needs at least 2 size t, a name (at least 2 char) and a buffer of at least 1 byte
    if (length < sizeof(size_t) + sizeof(size_t) + 3) {
        return -EINVAL;
    }

    dbg("sizeof size_t %lu\n", sizeof(size_t));
    if (debug) {
        for (i = 0; i < length; i++) {
            printk("%d ", (int)buffer[i]);
        }
    }

    copy_from_user(&namelen, buffer, sizeof(size_t));
    copy_from_user(&buflen, (buffer+sizeof(size_t)), sizeof(size_t));

    dbg("Gor namelen = %lu and buflen = %lu\n", namelen, buflen);

    if (buflen <= 0 || namelen <= 0) return -EINVAL;
    if ( (namelen + buflen + 2*sizeof(size_t)) < length) return -EINVAL;

    id = kmalloc(namelen+1, GFP_KERNEL);
    if (!id) {
        err("Ooops\n");
        return -EINVAL;
    }

    memset(id, 0, namelen+1);

    if (strncpy_from_user(id, buffer+2*sizeof(size_t), namelen) < namelen) {
        kfree(id);
        err("Could not copy from user\n");
        return -EINVAL;
    }

    dbg("looking for id %s\n", id);

    cur_ctx = find_ctx(id);
    if (cur_ctx) {
        saved_ctx_release(cur_ctx);
        list_del(&cur_ctx->to_list);
        kfree(id);
        dbg("Found version already on the list!\n");
    } else {
        dbg("New entry\n");
        cur_ctx = kmalloc(sizeof(struct saved_ctx), GFP_KERNEL);
        if (!cur_ctx) {
            kfree(id);
            err("Ooops 2\n");
            return -EINVAL;
        }
        saved_ctx_init(cur_ctx);
        cur_ctx->id = id;
    }

    if (saved_ctx_reserve(cur_ctx, buflen)) {
        kfree(cur_ctx->id);
        kfree(cur_ctx);
        err("OOOps 3\n");
        return -EINVAL;
    }

    retval = kfifo_from_user(&cur_ctx->fifo,
                                 buffer + 2*sizeof(size_t) + namelen,
                                 buflen,
                                 &copied);

    dbg("Got %d bytes from user\n", (int)copied);

    if (retval || (copied < buflen)) {
        kfree(cur_ctx->id);
        kfifo_free(&cur_ctx->fifo);
        kfree(cur_ctx);
        return retval;
    }

    cur_ctx->len = buflen;

    add_saved_context(cur_ctx);
    dbg("Wapping up\n");
    return 2*sizeof(size_t) + namelen + buflen;
}

/* Declare the sysfs entries. The macros create instances of dev_attr_fifo and dev_attr_reset */
    //static DEVICE_ATTR(fifo, S_IWUSR, NULL, sys_add_to_fifo);
    //static DEVICE_ATTR(reset, S_IWUSR, NULL, sys_reset);

static int meta_open(struct inode *inode, struct file *filp) {

    if (MINOR(filp->f_inode->i_rdev) == 0) {
        return saveme_device_open(inode, filp);
    } else
        return restoreme_device_open(inode, filp);
}
static int meta_close(struct inode* inode, struct file* filp) {
    if (MINOR(filp->f_inode->i_rdev) == 0) {
        return saveme_device_close(inode, filp);
    } else
        return restoreme_device_close(inode, filp);
}

static ssize_t meta_write(struct file* filp, const char __user *buffer, size_t length, loff_t* offset) {
    if (MINOR(filp->f_inode->i_rdev) == 0) {
        return saveme_device_write(filp, buffer, length, offset);
    } else
        return restoreme_device_write(filp, buffer, length, offset);
}

static ssize_t meta_read(struct file* filp, char __user *buffer, size_t length, loff_t *offset) {
    if (MINOR(filp->f_inode->i_rdev) == 0) {
        return -EINVAL;
    } else
        return restoreme_device_read(filp, buffer, length, offset);
}

static struct file_operations ctxsave_fops = {
read: meta_read,
write: meta_write,
open: meta_open,
release: meta_close
};


/* Module initialization and release */
static int __init ctxsave_module_init(void)
{
    int retval;
    dbg("\n");

    /* First, see if we can dynamically allocate a major for our device */
    ctxsave_major = register_chrdev(0, DEVICE_NAME, &ctxsave_fops);
    if (ctxsave_major < 0) {
        err("failed to register device: error %d\n", ctxsave_major);
        retval = ctxsave_major;
        goto failed_chrdevreg;
    }

    /* We can either tie our device to a bus (existing, or one that we create)
     * or use a "virtual" device class. For this example, we choose the latter */
    ctxsave_class = class_create(THIS_MODULE, CLASS_NAME);
    if (IS_ERR(ctxsave_class)) {
        err("failed to register device class '%s'\n", CLASS_NAME);
        retval = PTR_ERR(ctxsave_class);
        goto failed_classreg;
    }

    /* With a class, the easiest way to instantiate a device is to call device_create() */
    saveme_device = device_create(ctxsave_class, NULL, MKDEV(ctxsave_major, 0), NULL, DEVICE_NAME_SAVEME);
    if (IS_ERR(saveme_device)) {
        err("failed to create device '%s_%s'\n", CLASS_NAME, DEVICE_NAME_SAVEME);
        retval = PTR_ERR(saveme_device);
        goto failed_devreg_save;
    }

    restoreme_device = device_create(ctxsave_class, NULL, MKDEV(ctxsave_major, 1), NULL, DEVICE_NAME_RESTOREME);
    if (IS_ERR(restoreme_device)) {
        err("failed to create device '%s_%s'\n", CLASS_NAME, DEVICE_NAME_RESTOREME);
        retval = PTR_ERR(restoreme_device);
        goto failed_devreg_restore;
    }

    mutex_init(&restoreme_device_mutex);
    INIT_LIST_HEAD(&saved_ctx_list);

    return 0;

failed_devreg_restore:
    device_destroy(ctxsave_class, MKDEV(ctxsave_major, 1));
failed_devreg_save:
    device_destroy(ctxsave_class, MKDEV(ctxsave_major, 0));
    class_destroy(ctxsave_class);
failed_classreg:
    unregister_chrdev(ctxsave_major, DEVICE_NAME);
failed_chrdevreg:
    return -1;
}

static void __exit ctxsave_module_exit(void)
{
    struct list_head *cur, *next;
    dbg("\n");
    mutex_lock(&restoreme_device_mutex);

    list_for_each_safe(cur, next, &saved_ctx_list) {
        struct saved_ctx * ctx = list_entry(cur, struct saved_ctx, to_list);
        saved_ctx_release(ctx);
        kfree(ctx->id);
        list_del(&ctx->to_list);
        kfree(ctx);
    }
    mutex_destroy(&restoreme_device_mutex);

    device_destroy(ctxsave_class, MKDEV(ctxsave_major, 0));
    device_destroy(ctxsave_class, MKDEV(ctxsave_major, 1));
    class_destroy(ctxsave_class);
    unregister_chrdev(ctxsave_major, DEVICE_NAME);
}

/* Let the kernel know the calls for module init and exit */
module_init(ctxsave_module_init);
module_exit(ctxsave_module_exit);
