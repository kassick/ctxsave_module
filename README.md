# CtxSave Module

Code for a guided-exercise for an Operating Systems course.

This example showcases how one can use modules and virtual files to extend the functionality of the operating system.

The provided functionality (applications asking the O.S. to store some data that they can request when executed again) could obviously be implemented in userspace using files.
